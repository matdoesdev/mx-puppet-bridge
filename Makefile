all: prepare build

.PHONY: release release-draft update-lock build lint pre-commit prepare CHANGELOG.md test coverage start pack clean

# Deduce the current version
VERSION=$(shell jq -r .version package.json)

# Common utilities
YARN=./yarn
TOWNCRIER=python -m towncrier

# All .ts files
TSFILES=$(shell find src/ test/ -iname '*.ts')

# Release mode
RELEASE?=minor

include make/*.mk

## Install immutably, and mark the task
.yarn.install: package.json yarn.lock
	$(YARN) install --immutable
	@touch $@

# Install just installs immutably, used by CI
install: .yarn.install

## Update lock file if package.json has changed
yarn.lock: package.json
# Skip regular install when in CI as we only want --immutable installs
# but for local development, it's convenient that build will also run yarn install
# if necessary
ifndef CI
	$(YARN) install
endif
	# Mark the task as done if package.json has been newer, but doesn't actually change lockfile
	@touch $@

# Update the lock file
update-lock: yarn.lock

# Build and lint depend on install and all .ts files
.yarn.build .yarn.lint .yarn.docs: .yarn.install $(TSFILES)

# Yarn test depends on sources being transpiled
.yarn.test: .yarn.build

# Common yarn tasks like: build, lint, install, test
.yarn.%: package.json
	$(YARN) $(patsubst .yarn.%,%,$@)
	@touch $@

# Expose steps for CI
build lint test: %: .yarn.%

.nyc_output/.coverage: .yarn.build
	$(YARN) coverage
	@touch $@

coverage/cobertura-coverage.xml: .nyc_output/.coverage
	$(YARN) nyc report --reporter=cobertura
	@touch $@

coverage: coverage/cobertura-coverage.xml

mx-puppet-bridge.tgz: .yarn.build
	$(YARN) pack --out $@

pack: mx-puppet-bridge.tgz

docs: .yarn.docs

clean:
	rm -rf .yarn.*
	rm -rf lib/
	rm -rf coverage/
	rm -rf .nyc_output/

realclean: clean
	rm -rf node_modules
