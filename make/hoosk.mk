.prepare:
	git config core.hooksPath .hooks
	@touch $@

# Prepare for git hooks
prepare: .prepare

### HOOKS
pre-commit: lint
