/*
Copyright 2018 matrix-appservice-discord

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as Database from "better-sqlite3";
import { Log } from "../log";
import {
	IDatabaseConnector,
	IDatabaseTransaction,
	ISqlCommandParameters,
	ISqlRow,
} from "./connector";
import * as prometheus from "prom-client";

const log = new Log("SQLite3");

export class SQLite3Transaction implements IDatabaseTransaction {
	public type = "sqlite";
	private rolledBack = false;

	public constructor(private tx: Database.Database) {}

	public async Get(
		sql: string,
		parameters?: ISqlCommandParameters
	): Promise<ISqlRow | null> {
		log.silly("Get:", sql);
		return this.tx.prepare(sql).get(parameters || []);
	}

	public async All(
		sql: string,
		parameters?: ISqlCommandParameters
	): Promise<ISqlRow[]> {
		return this.tx.prepare(sql).all(parameters || []);
	}

	public async Run(
		sql: string,
		parameters?: ISqlCommandParameters,
		returnId?: string
	): Promise<number> {
		log.silly("Run:", sql);
		const info = this.tx.prepare(sql).run(parameters || []);
		return info.lastInsertRowid as number;
	}

	public async Exec(sql: string): Promise<void> {
		log.silly("Exec:", sql);
		this.tx.exec(sql);
	}
}

export class SQLite3 implements IDatabaseConnector {
	public type = "sqlite";
	public latency: prometheus.Histogram<string>;
	private db: Database.Database;
	private insertId: number;

	private txCount = 0;

	constructor(private filename: string) {
		this.insertId = -1;
		this.latency = new prometheus.Histogram({
			name: "bridge_database_query_seconds",
			help: "Time spent querying the database engine",
			labelNames: ["protocol", "engine", "type", "table"],
			// eslint-disable-next-line  no-magic-numbers
			buckets: [0.002, 0.005, 0.0075, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 5],
		});
	}

	public async Transaction<T>(
		callback: (txn: IDatabaseTransaction) => Promise<T>
	): Promise<T> {
		const id = `tx_${++this.txCount}`;
		const tx = new SQLite3Transaction(this.db);
		let result: T;
		// do our own transaction handling as better-sqlite3 does not have
		// promise support
		try {
			this.db.exec(`SAVEPOINT ${id}`);
			result = await callback(tx);
		} catch (e) {
			log.error("exception", e);
			this.db.exec(`ROLLBACK TO ${id}`);
			throw e;
		}
		this.db.exec(`RELEASE ${id}`);
		return result;
	}

	public async Open() {
		log.info(`Opening ${this.filename}`);
		this.db = new Database(this.filename);
	}

	public async Get(
		sql: string,
		parameters?: ISqlCommandParameters
	): Promise<ISqlRow | null> {
		log.silly("Get:", sql);
		return this.db.prepare(sql).get(parameters || []);
	}

	public async All(
		sql: string,
		parameters?: ISqlCommandParameters
	): Promise<ISqlRow[]> {
		log.silly("All:", sql);
		return this.db.prepare(sql).all(parameters || []);
	}

	public async Run(
		sql: string,
		parameters?: ISqlCommandParameters,
		returnId?: string
	): Promise<number> {
		log.silly("Run:", sql);
		const info = this.db.prepare(sql).run(parameters || []);
		return info.lastInsertRowid as number;
	}

	public async Close(): Promise<void> {
		this.db.close();
	}

	public async Exec(sql: string): Promise<void> {
		log.silly("Exec:", sql);
		this.db.exec(sql);
	}
}
